import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BannedPage } from './banned.page';

describe('BannedPage', () => {
  let component: BannedPage;
  let fixture: ComponentFixture<BannedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BannedPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BannedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
