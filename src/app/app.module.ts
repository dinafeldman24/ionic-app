import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpModule } from '@angular/http';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { FormsModule} from '@angular/forms';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal/ngx';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { CameraPreview, CameraPreviewOptions } from '@ionic-native/camera-preview/ngx';
import { Push} from '@ionic-native/push/ngx';
 
import { Camera } from '@ionic-native/Camera/ngx';
import { File } from '@ionic-native/File/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Base64ToGallery } from '@ionic-native/base64-to-gallery/ngx';
import { Facebook } from '@ionic-native/facebook/ngx';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';


import { IonicStorageModule } from '@ionic/storage';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthModule } from  './auth/auth.module';
import { BonusModule } from  './bonus/bonus.module';




@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [ BrowserModule,  HttpModule, IonicModule.forRoot(),AuthModule, BonusModule, FormsModule, HttpClientModule, AppRoutingModule, IonicStorageModule.forRoot()],
  providers: [
    StatusBar,
    SplashScreen,
    PayPal,
    Camera,
    CameraPreview,
    File,
    WebView,
    Base64ToGallery,
    Geolocation,
    FilePath,
    Push,
    Facebook,
    LocalNotifications,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy  },
    { provide: LocationStrategy, useClass: PathLocationStrategy }

  ],
  
  bootstrap: [AppComponent]
})
export class AppModule {}
