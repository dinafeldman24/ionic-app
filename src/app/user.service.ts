import { Injectable } from '@angular/core';
import { Storage } from  '@ionic/storage';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './auth/auth.service';
import { toArray } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  data:any;
  SERVER_ADDRESS:  string  =  'https://dev1-itsbonustime.tigerchef.com/api/';

  constructor(private  httpClient:  HttpClient, public  storage:  Storage, private authService:AuthService) { }


  load() {
    return new Promise(resolve => {
    let headers = new HttpHeaders();
    this.authService.storage.get('id').then((val) => {
       this.httpClient.get(`${this.SERVER_ADDRESS}users.php?action=account_info&userid=${val}`, {headers}).pipe(toArray())
      .subscribe(data => {
        this.data = data;
        resolve(this.data);
      });
  });
   
});

}


}
