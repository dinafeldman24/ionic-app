import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Push, PushObject, PushOptions } from '@ionic-native/push/ngx';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './auth/auth.service';
import { Router } from '@angular/router';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { UserService } from './user.service';





@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private push: Push,
    private  httpClient:  HttpClient,
    private  authService:  AuthService,
    private router: Router,
    private localNotifications: LocalNotifications,
    private userService: UserService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleLightContent();
      this.splashScreen.hide();
      this.setupPush();
    });
    
    this.platform.backButton.subscribe(() => {
      this.authService.storage.get('id').then((val) => {
        if (val > 0){
          this.router.navigate(['tabs/deals']);
        }
        else {
          this.router.navigate(['welcome']);
        }
      });
    });

    this.platform.backButton.subscribeWithPriority(9, () => {
      this.authService.storage.get('id').then((val) => {
        if (val > 0){
          this.router.navigate(['tabs/deals']);
        }
        else {
          this.router.navigate(['welcome']);
        }
      });
    });

     this.userService.load()
    .then(data => {
     if (data[0].message)
        {
          
        }
      else {
        this.authService.storage.set("id", data[0].id);
        this.authService.storage.set("is_banned",data[0].is_banned);
      }
    });

  }

  setupPush(){
    const options: PushOptions = {
      android: {
        senderID:'787563302081'
      },
      ios: {
          alert: 'true',
          badge: true,
          sound: 'false'
      }
   }
   
   const pushObject: PushObject = this.push.init(options);
   pushObject.on('notification').subscribe((notification: any) => {
            console.log('Received a notification', notification);
            this.localNotifications.schedule({
              text: notification.message,
              title: 'Bonus'
            });
   });

   pushObject.on('registration').subscribe((registration: any) => 
           { 
             console.log('device_id', registration.registrationId);
             this.authService.storage.set("device_id", registration.registrationId)
           }
   );
   
   pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));
  }

}
  


