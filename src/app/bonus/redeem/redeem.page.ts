import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { Router } from '@angular/router';
import { TabsPage } from '../tabs/tabs.page';
import { UserService } from '../../user.service';



@Component({
  selector: 'app-redeem',
  templateUrl: './redeem.page.html',
  styleUrls: ['./redeem.page.scss'],
})
export class RedeemPage {
  
  
  constructor( private tabs: TabsPage, private  authService:  AuthService,private router: Router,private userService: UserService) { 

  }

 

  ionViewWillEnter() {
    this.tabs.changeTabInContainerPage(3);
    this.userService.load()
    .then(data => {
     if (data[0].message)
        {
          
        }
      else {
        this.authService.storage.set("id", data[0].id);
        this.authService.storage.set("is_banned",data[0].is_banned);
      }
    });
    this.authService.storage.get('is_banned').then((val) => {
       if (val==='Y')
         this.router.navigateByUrl('/banned');
        });      
   }

  goToCamera(){ 
        this.router.navigate(['tabs/new-scan']);
  }

  

}  


  
