import { Component, OnInit, ViewChild } from '@angular/core';
import { ItemsService } from '../../items.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { toArray, max } from 'rxjs/operators';
import { AlertController } from '@ionic/angular';
import { IonContent } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthService } from '../../auth/auth.service';
import { NavController, IonSearchbar } from '@ionic/angular';
import { TabsPage } from '../tabs/tabs.page';





@Component({
  selector: 'app-deals',
  templateUrl: './deals.page.html',
  styleUrls: ['./deals.page.scss'],
})
export class DealsPage implements OnInit {
  public items:any;
  public dataList:any;
  public message:any;
  public coupon_result:any;
  public button_clicked:boolean=false;
  public imagepath :string ='https://dev1-itsbonustime.tigerchef.com/assets/item_images/';
  SERVER_ADDRESS:  string  =  'https://dev1-itsbonustime.tigerchef.com/api/';
  @ViewChild('deals', { static: false }) deals; 
  @ViewChild(IonContent, { static: true }) content: IonContent;
  @ViewChild('dealSearchbar', { static: true }) searchbar: IonSearchbar;
  sub:any;
  sorting_on:boolean=false;
  sortby:string='uses_left';
  sortype:string='DESC';
  filteredList:any;
  userid:number;
  totalitems :number;
  showed:number = 0;
  brands:any;
  cats:any;
  minprice:number;
  maxprice:number;
  minexp:any;
  maxexp:any;
  filtered_price:any={'lower': 0,'upper':0};
  filtered_exp:any={'lower': 0,'upper':0};
  filtered_brand:any;
  filtered_cats:any;
  showClearFilter:boolean=false;
  couponList:any;
  dataAlerts:any;
  showBrands:boolean=false;
  showCats:boolean=false;
  showPrice:boolean=false;
  showExp:boolean=false;
  showSort:boolean=false;
  overlayHidden: boolean = true;

  constructor(public storage: Storage, private tabs: TabsPage, public navCtrl: NavController, private route: ActivatedRoute, public alertController: AlertController, private  httpClient:  HttpClient, public itemsService: ItemsService,private router: Router, private  authService:  AuthService) { 
    this.dataList = [];
    this.filteredList=[];
    this.filtered_brand=[];
    this.filtered_cats=[];
    this.cats=[];
    this.couponList = [];
    this.items=[];
   }

  async ngOnInit() {
    await this.authService.storage.get('id').then((val) => {
      this.userid = val; 
      this.loadItems(this.userid);
    });
  }

  
  async loadItems(userid){
    await this.itemsService.load(userid,0)
    .then(data => {
     if (data[0].message)
        {
          this.message = 'No active deals found';
        }
      else {
        this.items=data[0]; 
          for(let i=0;i<this.items.length;i++){
            if (this.items[i].cats) {
            let tempcats= this.items[i].cats.split(',');
            this.cats = this.cats.concat(tempcats);
          } 
        }
        this.sort(this.sortby,this.sortype);
        this.resetDataList(this.items);
      }
    });
  }
  ViewCoupon(item) {
    if (this.button_clicked==false){
        //this.router.navigate(['tabs/deal-details', item]);
        item.came_from='deals';
        this.router.navigate(['tabs/deal-details', item]);
    }
    this.button_clicked=false;    
  }

  

  uses_left (limit, uses){
    return this.itemsService.uses_left(limit, uses);
  }

  addCoupon(deal_id:number){
    let message:string;
    message='Coupon was added to your list';
    this.button_clicked=true;
    let headers = new HttpHeaders();
    this.authService.storage.get('id').then((val) => {
      this.httpClient.get(`${this.SERVER_ADDRESS}items.php?action=addcoupon&dealid=${deal_id}&userid=${val}`, {headers}).pipe(toArray())
      .subscribe(data => {
        this.coupon_result = data[0]['message'];
       });
  });
  for (let i=0;i<this.items.length;i++){
        if (this.items[i].id==deal_id){
            this.items[i].inlist='Y';
        }
  }
  this.addedAlert(message);
  }

  renewCoupon(coupon_id:number){
    let message:string;
    message='Coupon Renewed';
    this.button_clicked=true;
    let headers = new HttpHeaders();
    this.authService.storage.get('id').then((val) => {
      this.httpClient.get(`${this.SERVER_ADDRESS}items.php?action=renewcoupon&couponid=${coupon_id}&userid=${val}`, {headers}).pipe(toArray())
      .subscribe(data => {
        this.coupon_result = data[0]['message'];
       });
  });
  for (let i=0;i<this.items.length;i++){
        if (this.items[i].coupon_id==coupon_id){
            this.items[i].hours_past=0;
        }
  }
  this.addedAlert(message);
  }
  
  async addedAlert(message:string) {
    const alert = await this.alertController.create({
      header: '',
      subHeader: '',
      cssClass: 'alertCss',
      message: message,
      buttons: [{text:'CLOSE',cssClass:'alertButton'}]
    });

    await alert.present();
  }

  loadData(event) {
    setTimeout(() => {
      if (this.filteredList.length > 0) {
           for (let i = this.showed; i < this.showed+8; i++) { 
              this.dataList.push(this.filteredList[i]);
          }
     event.target.complete();
     this.showed +=8;
    
      /*if (this.dataList.length >= this.filteredList.length) {
            event.target.disabled = true;
          }    */
      }
     else {
        for (let i = this.showed; i < this.showed+8; i++) { 
          this.dataList.push(this.items[i]);
        }
     
      event.target.complete();
      this.showed +=8;
      /*if (this.dataList.length >= this.items.length) {
        event.target.disabled = true;
      }*/
    } 
    }, 50);
  }
  

dynamicSort(property) {
  var sortOrder = 1;
  if(property[0] === "-") {
      sortOrder = -1;
      property = property.substr(1);
  }
  return function (a,b) {
      var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
      return result * sortOrder;
  }
}



sortList(){
/*  if (this.sorting_on==true)
        this.sorting_on=false;
  else {
          this.sorting_on=true;      
          this.sort(this.sortby, this.sortype);
        }   */
  this.content.scrollToTop();
  this.sorting_on=true;
}

sort(sortby,sortype){
  this.sortby=sortby; 
  this.sortype=sortype;
  if (this.filteredList.length > 0) {
    if (this.sortype=='asc')
        this.filteredList.sort(this.dynamicSort(sortby));
    else      
        this.filteredList.sort(this.dynamicSort("-"+sortby));
    
        this.resetDataList(this.filteredList);        
  }
else {  
  if (this.sortype=='asc')
        this.items.sort(this.dynamicSort(sortby));
  else      
        this.items.sort(this.dynamicSort("-"+sortby));
  this.resetDataList(this.items);
}
}

togglesort(sortby){
  if (this.sortype=='asc') this.sortype='dsc';
  else this.sortype='asc';
  this.sort(sortby, this.sortype);

}

search(ev, searchterm){
  let keyword:string;
  if (searchterm) keyword = searchterm.toLowerCase();
  else {
        keyword = ev.target.value;
        keyword = keyword.toLowerCase();
  }  
  if (this.filteredList.length > 0) {
      this.filteredList=this.filteredList.filter((item) => {
      let brand=item.client_name.toLowerCase();
      let name=item.item_name.toLowerCase();
      if (brand.indexOf(keyword) >= 0 ||
          name.indexOf(keyword) >= 0) {
        return true
      }
      return false
    });

  }
  else {  
    this.filteredList=this.items.filter((item) => {
    let brand=item.client_name.toLowerCase();
    let name=item.item_name.toLowerCase();
    if (brand.indexOf(keyword) >= 0 ||
        name.indexOf(keyword) >= 0) {
      return true
    }
    return false
  });
  }
  this.resetDataList(this.filteredList);

}

resetDataList(array){
  this.dataList = [];
  if (array){
    for (let i = 0; i < 8 && i < array.length; i++) { 
        this.dataList.push(array[i]);
    }  
  }  
  this.showed=8;
}

cancelSearch(){
  this.resetDataList(this.items); 
  this.searchbar.value='';
  this.filter_list();
}

filterOn(){
  this.overlayHidden = false;
  this.content.scrollToTop();
  this.resetDataList(this.items);
  this.brands = this.items.map(item => item.client_name).filter((value, index, self) => self.indexOf(value) === index);
  this.cats = this.cats.filter(function(elem, index, self) {return index === self.indexOf(elem);});
  let pricearray=this.getpricearray();
  this.minprice=this.getMin(pricearray);
  this.maxprice=this.getMax(pricearray);
  let mindate=new Date(Math.min.apply(null, this.items.map(function(e) {
    return new Date(e.end_date);
  })));
  let maxdate=new Date(Math.max.apply(null, this.items.map(function(e) {
    return new Date(e.end_date);
  })));
  let today:any=new Date(new Date().setHours(0,0,0,0));
  this.minexp= Math.round((mindate.getTime() - today)/86400000);
  this.maxexp= Math.round((maxdate.getTime() - today)/86400000);
  this.filtered_price.lower=this.minprice;
  this.filtered_exp.lower=this.minexp;
  this.filtered_price.upper=this.maxprice;
  this.filtered_exp.upper=this.maxexp;
  }

getMin(data){
  
  return Math.min.apply(null, data);
}
getMax(data){
  return Math.max.apply(null, data);
}

getpricearray(){
  return this.items.map(d => parseFloat(d.coupon_value));
}

getdatearray(){
  return this.items.map(d => d.end_date);
}

filter_list(){
   let brand_string:string='';
   let selected_cats=[];
   let cat_string:string='';
   this.message='';
   let today:any=new Date(new Date().setHours(0,0,0,0));
   
   for (let i=0;i<this.brands.length;i++){
      if (this.filtered_brand[i]==true)
          brand_string +=this.brands[i]+" ";
   }
   
     
   for (let i=0;i<this.cats.length;i++){
    if (this.filtered_cats[i]===true)
          cat_string=cat_string+this.cats[i]+",";
    }
    cat_string = cat_string.slice(0, -1); // remove last ,

    if (this.filtered_cats.length > 0)
          selected_cats=cat_string.split(',');

    if (selected_cats.length > 0 && brand_string.length > 0){
    this.filteredList=this.items.filter((item) => {
    let tempcats;
    let end_date= new Date(item.end_date);
    let end_date_diff=Math.round((end_date.getTime() - today)/86400000); 
    if (item.cats) {
          tempcats= item.cats.split(',');
    }
   
    if ((brand_string.indexOf(item.client_name) >= 0 && this.hasSameValue(tempcats, selected_cats)) 
        && (item.coupon_value >= (this.filtered_price.lower) && item.coupon_value <= this.filtered_price.upper)
        && (end_date_diff >= (this.filtered_exp.lower) && end_date_diff <= this.filtered_exp.upper)
        ) {
       return true
     }
     return false
   })
   }

  else if (selected_cats.length > 0 ){
    this.filteredList=this.items.filter((item) => {
    let end_date= new Date(item.end_date);
    let end_date_diff=Math.round((end_date.getTime() - today)/86400000); 
    let tempcats;
    if (item.cats) {
      tempcats= item.cats.split(',');
    }
    
    if ((this.hasSameValue(tempcats, selected_cats))
        && (item.coupon_value >= (this.filtered_price.lower) && item.coupon_value <= this.filtered_price.upper)
        && (end_date_diff >= (this.filtered_exp.lower) && end_date_diff <= this.filtered_exp.upper)
        ) {
       return true
     }
     return false
   })
   }

 
   else if (brand_string.length > 0){
   this.filteredList=this.items.filter((item) => {
   let end_date= new Date(item.end_date);
   let end_date_diff=Math.round((end_date.getTime() - today)/86400000); 
   if ((brand_string.indexOf(item.client_name) >= 0 )
       && (item.coupon_value >= (this.filtered_price.lower) && item.coupon_value <= this.filtered_price.upper)
       && (end_date_diff >= (this.filtered_exp.lower) && end_date_diff <= this.filtered_exp.upper)
       ) {
      return true
    }
    return false
  })
  }

  else { 
    this.filteredList=this.items.filter((item) => {
      let end_date= new Date(item.end_date);
      let end_date_diff=Math.round((end_date.getTime() - today)/86400000); 
      if ((item.coupon_value >= (this.filtered_price.lower) && item.coupon_value <= this.filtered_price.upper)
          && (end_date_diff >= (this.filtered_exp.lower) && end_date_diff <= this.filtered_exp.upper)
          ) {
         return true
       }
       return false
     })
  }  

   if (this.filteredList.length===0){
        this.message="No deals were found for this filter combination";
   }
   if (!this.searchbar.value)
          this.resetDataList(this.filteredList);
   else this.search('',this.searchbar.value); 
   this.overlayHidden = true;
}

close_filter(){
  this.overlayHidden = true;
}


clear_filter(){
  for (let i=0;i<this.filtered_brand.length;i++){
       this.filtered_brand[i]=false;
  }     
  for (let i=0;i<this.filtered_cats.length;i++){
    this.filtered_cats[i]=false;
}     
  this.message='';
  this.filtered_price={'lower': this.minprice,'upper':this.maxprice};
  this.filtered_exp={'lower': this.minexp,'upper':this.maxexp};
  this.showClearFilter=false;
  this.cancelSearch();
  this.overlayHidden = true;
  }

   hasSameValue(arr1, arr2) {
    let found = false;
    if (!arr1 || !arr2) return false;
    for (let i = 0; i < arr1.length; i++) {
     for (let j = 0; j < arr2.length; j++) {
       if (arr1[i].indexOf(arr2[j]) !== -1) {
          found = true;
        break;
      }
     } 
    }
    return found;
  }

  ionViewWillEnter() {
    this.authService.storage.get('is_banned').then((val) => {
       if (val==='Y')
         this.router.navigateByUrl('banned');
        });      
    if (this.filteredList.length > 0) {
          this.resetDataList(this.filteredList); 
        }  
    else this.resetDataList(this.items);        
   }
 
scrollToTop() {
  this.content.scrollToTop(400);
}
async ionViewDidEnter(){
  this.tabs.changeTabInContainerPage(1);
  this.scrollToTop();
  await this.authService.storage.get('id').then((val) => {
    this.userid = val; 
    this.loadItems(this.userid);
  });
  this.sub = this.route.params.subscribe(params => {
    this.updateItems(params);
  });
  this.resetDataList(this.items); 
} 

updateItems(item){
  let changedid = item['id']; 
  for (let i=0;i<this.items.length;i++){
    if (this.items[i].id==changedid)
    {
        this.items[i].inlist=item['inlist'];
        this.items[i].has_alert=item['has_alert'];
    }
  }
}

 show_brands(){
  this.showBrands=!this.showBrands;
  this.showCats=false;
  this.showPrice=false;
  this.showSort=false;
 }

 show_cats(){
  this.showCats=!this.showCats;
  this.showBrands=false;
  this.showPrice=false;
  this.showExp=false;
  this.showSort=false;
 }

 show_price(){
  this.showPrice=!this.showPrice;
  this.showCats=false;
  this.showBrands=false;
  this.showExp=false;
  this.showSort=false;
 }

 show_exp(){
  this.showExp=!this.showExp;
  this.showCats=false;
  this.showBrands=false;
  this.showPrice=false;
  this.showSort=false;
}  

show_sort(){
  this.showSort=!this.showSort;
  this.showCats=false;
  this.showBrands=false;
  this.showPrice=false;
  this.showExp=false;
}

 public hideOverlay() {
  this.overlayHidden = true;
}

}
