import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../../auth/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { TabsPage } from '../tabs/tabs.page';
import { FormControl, FormGroup, Validators,ValidatorFn,AbstractControl, ReactiveFormsModule } from '@angular/forms';
import { ToastController } from '@ionic/angular';


@Component({
  selector: 'app-results',
  templateUrl: './results.page.html',
  styleUrls: ['./results.page.scss'],
})
export class ResultsPage implements OnInit {
  infoIn:boolean=false;
  rid:string;
  filename:string;
  message:string='';
  confirmed_r:boolean=false;
  coupons:any =[];
  coupon_sum:any=0;
  add_review:boolean=false;
  public new_review:FormGroup=new FormGroup({
    action: new FormControl(''),
    comments: new FormControl('',[Validators.required]),
    userid: new FormControl(''),
    files: new FormControl(''),
  });

  public imagepath :string ='https://dev1-itsbonustime.tigerchef.com/assets/item_images/';
  
  constructor(private toastController: ToastController, private tabs: TabsPage, private route: ActivatedRoute, private  http:  HttpClient, private  authService:  AuthService, private router: Router) { 
  
  }

  async ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if (params && params.image) {
        this.filename = params.image;
        this.authService.storage.get('id').then((val) => { 
          this.getData(val);
      });
      }
    });
 }

  async getData(id) {
     if (id > 0){
      const formData = new FormData();
      formData.append('id', id);
      formData.append('filename', this.filename);
      this.http.post("https://dev1-itsbonustime.tigerchef.com/api/getinfo.php", formData)       
          .subscribe(res => {
             this.infoIn=true;
             if(res['message'])
             {
                this.message=res['message'];
             }
             
             else if (res['coupons'].length > 0) {
               this.coupons=res['coupons'];
               for(let i=0; i < this.coupons.length; i++)
               {
                  this.coupons[i].coupon_total=parseFloat(this.coupons[i].coupon_value) * parseInt(this.coupons[i].coupon_uses);
                  this.coupons[i].coupon_total=parseFloat(this.coupons[i].coupon_total).toFixed(2);
                  this.coupon_sum +=parseFloat(this.coupons[i].coupon_total);
               }
               this.coupon_sum=parseFloat(this.coupon_sum).toFixed(2);
             }

          });
      }
      else console.log ("something went worng");
    }

    ionViewWillEnter() {
      this.confirmed_r=false;
      this.message='';
      this.infoIn=false;
      this.rid='';
      this.coupons=[];
      this.coupon_sum=0;
      this.add_review=false;
      this.authService.storage.get('is_banned').then((val) => {
         if (val==='Y')
           this.router.navigateByUrl('/banned');
          });  
        this.tabs.changeTabInContainerPage(4);     
     }  
    
  confirmed(){
    this.confirmed_r=true;
  }   

  goshopping(){
    this.router.navigateByUrl('tabs/deals');
  }
  review(){
     this.message=' ';
     this.add_review=true;
  }

  
  send_review(form){
    this.authService.send_review(form.value).subscribe((res) => {
    if (res.message=='fail'){
            this.presentToast('Failed sending review');
    }
      
    if (res.message=='success'){
      this.presentToast('Your message was sent!');
      this.new_review.reset();
      this.router.navigateByUrl('tabs/deals');
    }
    
  });

  }


  adding_review() {
    this.new_review.controls['action'].setValue('add_review');
    this.new_review.controls['files'].setValue(this.filename);
    this.authService.storage.get('id').then((val) => {
      this.new_review.controls['userid'].setValue(val);
      if(this.new_review.valid){
          this.send_review(this.new_review);   
      }      
    });
    
  } 


  async presentToast(text) {
    
    const toast = await this.toastController.create({
        message: text,
        position: 'bottom',
        duration: 3000
    });
    toast.present();
  }

}  

