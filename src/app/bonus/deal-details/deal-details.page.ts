import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../../auth/auth.service';
import { ItemsService } from '../../items.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { toArray } from 'rxjs/operators';
import { AlertController } from '@ionic/angular';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { TabsPage } from '../tabs/tabs.page';




@Component({
  selector: 'app-deal-details',
  templateUrl: './deal-details.page.html',
  styleUrls: ['./deal-details.page.scss'],
})
export class DealDetailsPage implements OnInit {
  id:any;
  coupon_id:number;
  name:string;
  description:string;
  coupon_value : number;
  limit_per_user: number;
  coupon_uses:number;
  sub:any;
  end_date:any;
  soon:number;
  item_id:number;
  client_name:string;
  soon_date:string
  alerts:any;
  inlist:string;
  inAlertlist:boolean;
  couponList:any;
  has_alert:string;
  came_from:string="deals";
  hours_past:number;
  public imagepath :string ='https://dev1-itsbonustime.tigerchef.com/assets/item_images/';
  coupon_result:any;
  SERVER_ADDRESS:  string  =  'https://dev1-itsbonustime.tigerchef.com/api/';
  constructor(private tabs: TabsPage, private router: Router, public navCtrl: NavController, public alertController: AlertController, private  httpClient:  HttpClient, private  authService:  AuthService, public itemsService: ItemsService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id']; 
      this.name = params['item_name']; 
      this.description = params['description']; 
      this.coupon_value=params['coupon_value'];
      this.limit_per_user=params['limit_per_user'];
      this.coupon_uses=params['coupon_uses'];
      this.soon=params['soon'];
      this.end_date=params['end_date'];
      this.item_id=params['item_id'];
      this.client_name=params['client_name'];
      this.soon_date=params['soon_date'];
      this.inlist=params['inlist'];
      this.has_alert=params['has_alert'];
      this.came_from=params['came_from'];
      this.hours_past=params['hours_past'];
      this.coupon_id=params['coupon_id']
    });
    
  }



  uses_left (limit, uses){
    return this.itemsService.uses_left(limit, uses);
  }

  ionViewWillEnter() {
    this.authService.storage.get('is_banned').then((val) => {
       if (val==='Y')
         this.router.navigateByUrl('/banned');
        });  
      this.tabs.changeTabInContainerPage(1);     
   }

  addCoupon(deal_id:number){
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Basic " + btoa("tcbonus:bonus439"));
    this.authService.storage.get('id').then((val) => {
      this.httpClient.get(`${this.SERVER_ADDRESS}items.php?action=addcoupon&dealid=${deal_id}&userid=${val}`, {headers}).pipe(toArray())
      .subscribe(data => {
        this.coupon_result = data[0]['message'];
      });
  });
  this.addedAlert('Added Coupon','Coupon was added to your list');
  this.inlist='Y';
  }


  addAlert(item_id:number){
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Basic " + btoa("tcbonus:bonus439"));
    this.authService.storage.get('id').then((val) => {
      this.httpClient.get(`${this.SERVER_ADDRESS}items.php?action=addalert&itemid=${item_id}&userid=${val}`, {headers}).pipe(toArray())
      .subscribe(data => {
        this.coupon_result = data[0]['message'];
      });
  });
  this.inAlertlist=true;
  this.addedAlert('Added subscription','You are now subscribed to get alerts for this item');
  this.has_alert='Y';
 }
  
  async addedAlert(header:string, message:string) {
    const alert = await this.alertController.create({
      header: header,
      subHeader: '',
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }

  goback(){
    let item:any={
      'id':this.id,
      'inlist':this.inlist,
      'has_alert':this.has_alert
    }
    if (this.came_from=='deals')
        this.router.navigate(['tabs/deals', item]);
    else    
        this.router.navigate(['tabs/lists', item]);
  }


  removeAlert(item_id:number){
    let headers = new HttpHeaders();
    this.authService.storage.get('id').then((val) => {
      this.httpClient.get(`${this.SERVER_ADDRESS}items.php?action=removeAlert&itemid=${item_id}&userid=${val}`, {headers}).pipe(toArray())
      .subscribe(data => {
        this.coupon_result = data[0]['message'];
      });
  });
  this.has_alert='N';
  this.addedAlert('Removed subscription','You unsubscribed from this item');
 }

 renewCoupon(coupon_id:number){
  let message:string;
  message='Coupon Renewed';
  let headers = new HttpHeaders();
  this.authService.storage.get('id').then((val) => {
    this.httpClient.get(`${this.SERVER_ADDRESS}items.php?action=renewcoupon&couponid=${coupon_id}&userid=${val}`, {headers}).pipe(toArray())
    .subscribe(data => {
      this.coupon_result = data[0]['message'];
     });
  });
          this.hours_past=0;
          this.addedAlert('',message);
 }

}
