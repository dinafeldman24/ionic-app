import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../auth/auth.service';


@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.page.html',
  styleUrls: ['./welcome.page.scss'],
})
export class WelcomePage implements OnInit {
  
  
  constructor(private router: Router, private  authService:  AuthService) { }

  ngOnInit() {
    this.authService.storage.remove("is_banned");
  }

  ionViewWillEnter() {
    this.authService.storage.get('is_banned').then((val) => {
       if (val==='Y')
         this.router.navigateByUrl('/banned');
        });
   }

  
}
