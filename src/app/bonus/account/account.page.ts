import { Component, OnInit, Input, ViewChild,  ElementRef, Renderer2 } from "@angular/core";
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../../user.service';
import { Router } from '@angular/router';
import { AuthService } from '../../auth/auth.service';
import { TabsPage } from '../tabs/tabs.page';
import { ReceiptsService } from '../../receipts.service';
import { FormControl, FormGroup, Validators,ValidatorFn,AbstractControl, ReactiveFormsModule } from '@angular/forms';
import { ToastController } from '@ionic/angular';


@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class AccountPage  implements OnInit{
    public items: any = [];
    message:string='';
    public info:any;
    userid:number;
    public progress : number = 0;
    receipts:any=[];
    showAccount:boolean=true;
    showBilling:boolean=false;
    showPayout:boolean=false;
    showReceipt:boolean=false;
    openLogout:boolean=false;
    login_type:string='';
    editinfo=false;
    passMessage:string='';
    payoutInfo:any=[];      
    public updateInfo : FormGroup;
   
   constructor(public receiptsService: ReceiptsService, private tabs: TabsPage, private toastController: ToastController, private  authService:  AuthService, private router: Router, public renderer: Renderer2,private route: ActivatedRoute,public userService: UserService) {
      this.receipts=[];
      this.info=[];
      this.updateInfo = new FormGroup({
        email: new FormControl('', [Validators.required,Validators.email]),
        name: new FormControl('', [Validators.required]),
        action: new FormControl(''),
        login_type:new FormControl(''),
        id: new FormControl(''),
        current_password: new FormControl(''),
        password: new FormControl(''),
        repeat_password: new FormControl('', [this.equalto('password')]),
        }
      );
         
      
    }
    
    equalto(field_name): ValidatorFn {
      return (control: AbstractControl): {[key: string]: any} => {
      
      let input = control.value;
      
      let isValid=control.root.value[field_name]==input
      if(!isValid)
      return { 'equalTo': {isValid} }
      else
      return null;
      };
      }

      
    
    async ngOnInit() {
        await this.route.params.subscribe(params => {
        this.userid = params['id']; 
        this.loadReceipts();
        this.loadInfo();
      });
    
    }
    
    async loadReceipts(){
      await this.receiptsService.load()
      .then(data => {
       if (data)
          {
            this.receipts=data[0];
          }
        else {
            this.message="Failed to receive data, please try later";
          }
      });
    }

    ionViewWillEnter() {
      this.account_info();
      this.showAccount=true;
      this.passMessage="";
      this.authService.storage.get('login_type').then((val) => {
        this.login_type=val;
    });  
      this.tabs.changeTabInContainerPage(4);     
      this.authService.storage.get('is_banned').then((val) => {
         if (val==='Y')
           this.router.navigateByUrl('/banned');
          });      
      this.loadReceipts();
      this.loadInfo();
      
     }
  

    async loadInfo(){
      await this.userService.load()
      .then(data => {
       if (data[0].message)
          {
            this.message = 'No Information';
          }
        else {
          this.info=data[0];  
          this.payoutInfo=[];
          this.progress=this.getProgressValue(this.info.current_balance);
          for (var key in this.info.transactions) { 
                  if(this.info.transactions.hasOwnProperty(key)) { 
                      this.payoutInfo.push(this.info.transactions[key]);
                  }
          }
        }
      });
    }
  
    
    private getProgressValue(balance) {
      let minimum = 50;
         return balance/minimum;
    }     

    gotopaypal(){
      let navigationExtras: any = {
        queryParams: {
          frompage: 'account'
        }
      };
      this.router.navigate(['paypal'], navigationExtras);
    }
    
    account_info(){
        this.showAccount=!this.showAccount;
        this.showBilling=false;
        this.showPayout=false;
        this.showReceipt=false;
        this.openLogout=false;
    }

    billing_info(){
      this.showBilling=!this.showBilling;
      this.showAccount=false;
      this.showPayout=false;
      this.showReceipt=false;
      this.openLogout=false;
    }

    payout_history(){
      this.showAccount=false;
      this.showBilling=false;
      this.showReceipt=false;
      this.showPayout=!this.showPayout;
      this.openLogout=false;
  }

  show_rec(){
      this.showReceipt=!this.showReceipt;
      this.showAccount=false;
      this.showBilling=false;
      this.showPayout=false;
      this.openLogout=false;
  }

  logout(){
      this.openLogout=!this.openLogout;
      this.showReceipt=false;
      this.showAccount=false;
      this.showBilling=false;
      this.showPayout=false;
  } 

  editInfo(){
    this.passMessage="";
    this.editinfo=true;   
  }

  update(form){
    this.passMessage='';
    this.authService.updateInfo(form.value).subscribe((res) => {
    if (res.user.message=='fail'){
            this.presentToast('Failed to update');
    }
    else if (res.user.message=='invalidpass'){
      this.passMessage="Invalid password, information was not updated.";
    }
    else if (res.user.message=='nopass' && this.login_type=='pass'){
      this.passMessage="Password reqired, information was not updated.";
    }
    
    else {
      this.authService.storage.set("name",res.user.name);
      this.authService.storage.set("is_banned",res.user.is_banned);
      this.presentToast('Info Updated');
      this.editinfo=false; 
      }
    });
    this.updateInfo.controls.current_password.reset();
    this.updateInfo.controls.password.reset();
    this.updateInfo.controls.repeat_password.reset();

  }


  edit() {
    this.updateInfo.controls['action'].setValue('updateInfo');
    this.updateInfo.controls['login_type'].setValue(this.login_type);
    this.authService.storage.get('id').then((val) => {
      this.updateInfo.controls['id'].setValue(val);
      if(this.updateInfo.valid){
          this.update(this.updateInfo);   
      }      
    });
    
  } 

  async presentToast(text) {
    
    const toast = await this.toastController.create({
        message: text,
        position: 'bottom',
        duration: 3000
    });
    toast.present();
  }

  logoutNow(){
    this.authService.storage.remove("ACCESS_TOKEN");
    this.authService.storage.remove("RESPONSE");
    this.authService.storage.remove("email");
    this.authService.storage.remove("name");
    this.authService.storage.remove("id");
    this.authService.storage.remove("access_token");
    this.router.navigate(['welcome']);
  }
  
}