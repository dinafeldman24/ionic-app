import { Component, OnInit } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { AuthService } from '../../auth/auth.service';
import { Router } from '@angular/router';
import { TabsPage } from '../tabs/tabs.page';
import { ItemsService } from '../../items.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-stores',
  templateUrl: './stores.page.html',
  styleUrls: ['./stores.page.scss'],
})
export class StoresPage implements OnInit {
  public imagepath :string ='https://dev1-itsbonustime.tigerchef.com/assets/store_images/';
  latitude:number=0;
  longitude:number=0;
  stores:any=[];
  filtered:any;
  allstores:boolean=true;
  closestores:boolean=false;
  hasCloseStores:boolean=true;
  constructor(public itemsService: ItemsService, private geolocation: Geolocation, private  authService:  AuthService,private router: Router, private tabs: TabsPage, public alertController: AlertController) { }
  error:boolean[] = []
  ngOnInit() {
    this.itemsService.getStores()
    .then(data => {
          this.stores=data[0]; 
          this.filtered=this.stores;
        });
    }

  ionViewWillEnter() {
    this.tabs.changeTabInContainerPage(5);
    this.authService.storage.get('is_banned').then((val) => {
       if (val==='Y')
         this.router.navigateByUrl('/banned');
        });      
   }

   findstores(){
     this.allstores=false;
     this.closestores=true;
      this.geolocation.getCurrentPosition().then((resp) => {
      this.latitude=resp.coords.latitude;
      this.longitude=resp.coords.longitude;
      this.filtered=this.stores.filter((store) => {
        if (this.difference(store.latitude,this.latitude) < 0.862 && this.difference(store.longitude, this.longitude) < 0.862){
          return true;
        }
        else {
            return false;
        }    
      });
    }).catch((error) => {
          this.addedAlert()
    });
   
   }

   difference(n, m){
    return Math.abs(n - m)
   }

   viewall(){
    this.allstores=true;
    this.closestores=false;
    this.filtered=this.stores;
   }

   async addedAlert() {
    const alert = await this.alertController.create({
      header: 'Please share your location',
      subHeader: '',
      message: "To get a list of stores in your location, please allow sharing your loctaion in your device's setting" ,
      buttons: ['OK']
    });

    await alert.present();
  }

}
