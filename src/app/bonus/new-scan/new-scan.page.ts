import { CameraPreview, CameraPreviewPictureOptions, CameraPreviewOptions, CameraPreviewDimensions } from '@ionic-native/camera-preview/ngx';
import { Router } from '@angular/router';
import { AuthService } from '../../auth/auth.service';
import { TabsPage } from '../tabs/tabs.page';
import { ActionSheetController, ToastController, Platform, LoadingController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { AlertController } from '@ionic/angular';
import { Component, OnInit, ViewChild } from '@angular/core';
import { AngularCropperjsComponent } from 'angular-cropperjs';




@Component({
  selector: 'app-new-scan',
  templateUrl: './new-scan.page.html',
  styleUrls: ['./new-scan.page.scss'],
})

export class NewScanPage {
  @ViewChild('angularCropper',{ static: false }) public angularCropper: AngularCropperjsComponent;
  cameraPreviewOpts: CameraPreviewOptions;
  pictureOptions: CameraPreviewPictureOptions;
  picture:string=null;
  preview:boolean=false;
  startCamera:boolean=false;
  isLoading:boolean=false;
  latestScan:any;
  receiptId:string;
  image:any=[];
  info:any;
  topPreview:number=10;
  start_pic_frame:string ='10px';
  croppedImagepath:string = "";
  hasWriteAccess: boolean = false;
  data: any;
  scaleValX = 1;
  scaleValY = 1;
  cropperOptions:any;
  pic_frame_height:string="100%";
  pic_frame_padding:string='80px';
  image_count=0;
  filename:string='';
  
  
  constructor(public alertController: AlertController, private toastController: ToastController, private http: HttpClient, private loadingController: LoadingController, private tabs: TabsPage, private cameraPreview: CameraPreview, private router: Router, private  authService:  AuthService) { 
    
  let previewWidth=window.screen.width-20;
  let previewHeight=window.screen.height-110;
  
  this.cropperOptions = {
    aspectRatio: 0,
    autoCrop: true,
    movable: true,
    zoomable: true,
    scalable: true,
    autoCropArea: 0.8,
    dragCrop: true,
    guides:false,
    cropBoxMovable:true,
    cropBoxResizable:true,
    viewMode:3,
    dragMode: 'crop'
  };
  
  
   
  this.cameraPreviewOpts = {
    x: this.topPreview,
    y: 10,
    width: previewWidth,
    height:previewHeight,
    camera: 'rear',
    tapPhoto: false,
    previewDrag: false,
    toBack: true,
    alpha: 1,
    }
  
    this.pictureOptions = {
      quality:100,
      height:previewHeight*3,
      width:previewWidth*3,
    }

    
}

start(){
  this.showCameraView();
  this.picture='';
  console.log("going to start camerea");
  this.cameraPreview.startCamera(this.cameraPreviewOpts).then(
    (res) => {
     console.log(res);
    },
    (err) => {
      console.log(err)
    });
}

newPic(){
  this.cameraPreview.stopCamera();
  this.start();
}

async confirmPicture(){
  this.preview=true;
  this.isLoading = true;
  this.cameraPreview.stopCamera();
  await this.uploadImageData();
}

takePicture(){
  this.cameraPreview.takePicture(this.pictureOptions).then((imageData) => {
    this.picture = 'data:image/jpeg;base64,' + imageData;
    this.filename=Date.now().toString();
    this.image.push(this.filename);
    this.start_pic_frame='27px';
  }, (err) => {
     console.log('Problem accessing the camera ' + err);
  });
  this.cameraPreview.hide();
  this.showCropView();
}



exitCamera(){
  this.cameraPreview.hide();
  this.cameraPreview.stopCamera();
  this.router.navigate(['tabs/redeem']);
}


ionViewWillEnter(){
    this.authService.storage.get('is_banned').then((val) => {
    if (val==='Y')
      this.router.navigateByUrl('/banned');
     });      

    this.tabs.changeTabInContainerPage(6); 
    this.start();
    this.latestScan='';
    this.receiptId='';
    this.start_pic_frame='10px';
    this.image=[];
    this.filename='';
}    


async uploadImageData() {
  const formData = new FormData();
  const ocrData = new FormData();
  this.present();
  await this.authService.storage.get('id').then((val) => {
  formData.append('id', val);
  formData.append('scan',this.picture);
  formData.append('action','upload');
  formData.append('filename', this.image);
  ocrData.append('filename',this.image);
  ocrData.append('action','ocr');
  if (this.receiptId)
  {
    formData.append('rec_id', this.receiptId);
  }
   this.http.post("https://dev1-itsbonustime.tigerchef.com/api/uploadscan.php", formData)       
      .subscribe(res => {
          if (res['success']) {
              this.receiptId=res['rid'];
              formData.append('rec_id', this.receiptId);
              formData.append('latest', res['latest']);
              this.latestScan=res['latest'];
              this.multiReceipt();
                this.http.post("https://dev1-itsbonustime.tigerchef.com/api/uploadscan.php", ocrData)       
                .subscribe(res => {
                  console.log("ocr now");
              });  
              this.image_count++;
          } else {
             alert("Connection error, receipt was not uploaded");
          }
          console.log("sent to server");
      });
 });

}


 async present() {
   return await this.loadingController.create({
  }).then(a => {
    a.present().then(() => {
      console.log('presented');
      if (!this.isLoading) {
        a.dismiss().then(() => console.log('abort presenting'));
      }
    });
  });
}

async dismiss() {
 
  this.isLoading = false;
  return await this.loadingController.dismiss().then(() => console.log('dismissed'));
}

async presentToast(text) {
    
  const toast = await this.toastController.create({
      message: text,
      position: 'bottom',
      duration: 3000
  });
  toast.present();
}

async multiReceipt() {
  const alert = await this.alertController.create({
    header: 'Sending your Receipt',
    subHeader: '',
    message: 'Would you like to scan an additonal image?',
    backdropDismiss:false,
    buttons: [{
              'text':'Scan another image', 
               handler: () => {
                 this.additionalImage();
                  }
              }, 
              {'text':"Done",
              handler: () => {
                this.finishedScan();
              }
              }]
  });
  alert.present();
  this.dismiss();
}

additionalImage(){
  this.newPic();
}

finishedScan(){
  let params: any = {
    queryParams: {
      image: this.image
    }
  };
  this.router.navigate(['tabs/results'], params);   
}

 ionViewCanEnter(){
    this.start();   
 }

 ionViewWillLeave(){
  this.cameraPreview.hide();   
  this.cameraPreview.stopCamera();
 }


reset() {
  this.angularCropper.cropper.reset();
}

clear() {
  this.angularCropper.cropper.clear();
}

rotate() {
  this.angularCropper.cropper.rotate(90);
}

zoom(zoomIn: boolean) {
  let factor = zoomIn ? 0.1 : -0.1;
  this.angularCropper.cropper.zoom(factor);
}

scaleX() {
  this.scaleValX = this.scaleValX * -1;
  this.angularCropper.cropper.scaleX(this.scaleValX);
}

scaleY() {
  this.scaleValY = this.scaleValY * -1;
  this.angularCropper.cropper.scaleY(this.scaleValY);
}

move(x, y) {
  this.angularCropper.cropper.move(x, y);
}

save() {
   let croppedstring=this.angularCropper.cropper.getCroppedCanvas().toDataURL('image/jpeg', (100 / 100));
   this.picture=croppedstring;
   this.pic_frame_height='100%';
   this.pic_frame_padding='80px';
   this.preview=true;
}

showCameraView(){
  this.startCamera=true;
  this.pic_frame_height='100%';
  this.pic_frame_padding='80px';
  this.preview=true;
}

showCropView(){
  this.preview=false;
  this.pic_frame_height='0';
  this.pic_frame_padding='0';
  this.startCamera=false;
}


}
