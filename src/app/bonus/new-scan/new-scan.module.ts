import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { AngularCropperjsModule, AngularCropperjsComponent } from 'angular-cropperjs';



import { IonicModule } from '@ionic/angular';

import { NewScanPage } from './new-scan.page';

const routes: Routes = [
  {
    path: '',
    component: NewScanPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    AngularCropperjsModule,
  ],
  declarations: [NewScanPage]
}, )
export class NewScanPageModule {}
