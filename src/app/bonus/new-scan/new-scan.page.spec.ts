import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewScanPage } from './new-scan.page';

describe('NewScanPage', () => {
  let component: NewScanPage;
  let fixture: ComponentFixture<NewScanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewScanPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewScanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
