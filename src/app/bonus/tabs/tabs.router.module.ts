import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';
 
const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children:[
        { path: 'deals', loadChildren: '../deals/deals.module#DealsPageModule' },
        { path: 'lists', loadChildren: '../lists/lists.module#ListsPageModule' },
        { path: 'redeem', loadChildren: '../redeem/redeem.module#RedeemPageModule' },
        { path: 'account', loadChildren: '../account/account.module#AccountPageModule' },
        { path: 'stores', loadChildren: '../stores/stores.module#StoresPageModule' },
        { path: 'new-scan', loadChildren:'../new-scan/new-scan.module#NewScanPageModule' },
        { path: 'results', loadChildren: '../results/results.module#ResultsPageModule' },
       // { path: 'home', loadChildren: '../../home/home.module#HomePageModule' },
       // { path: 'signup', loadChildren: '../../auth/register/register.module#RegisterPageModule' },
       // { path: 'login', loadChildren: '../../auth/login/login.module#LoginPageModule' },
       // { path: 'forgotpass', loadChildren: '../../auth/forgotpass/forgotpass.module#ForgotpassPageModule' },
       // { path: 'welcome', loadChildren: '../welcome/welcome.module#WelcomePageModule' },
       //{ path: 'mainlogin', loadChildren: '../../mainlogin/mainlogin.module#MainloginPageModule' },
       // { path: 'paypal', loadChildren: '../../auth/paypal/paypal.module#PaypalPageModule' },
          { path: 'deal-details', loadChildren: '../deal-details/deal-details.module#DealDetailsPageModule' },
       // { path: 'banned', loadChildren: '../../banned/banned.module#BannedPageModule' },
    ]
  }
];
 
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}