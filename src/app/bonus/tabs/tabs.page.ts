import { Component, OnInit } from '@angular/core';
import {Injectable} from '@angular/core';
import { Observable} from 'rxjs';
import { Router } from '@angular/router';
import { AuthService } from '../../auth/auth.service';



@Injectable()

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {
  private tabChangeObserver: any;
  public tabChange: any;
  dealson :boolean =false;
  liston :boolean =false;
  redeemon :boolean =false;
  accounton :boolean =false;
  storeson :boolean=false;
  cameraon :boolean=false;


  constructor(private router: Router, private  authService:  AuthService) { 
    this.tabChangeObserver = null;
    this.tabChange = Observable.create(observer => {
        this.tabChangeObserver = observer;
    });
  }

  public changeTabInContainerPage(index: number) {
    this.dealson=false;
    this.liston=false;
    this.redeemon=false;
    this.storeson=false;
    this.accounton=false;
    this.cameraon=false;
    if (index==1)
        this.dealson=true;
    if (index==2)
        this.liston=true;        
    if (index==3)
        this.redeemon=true;      
    if (index==4)
        this.accounton=true;   
    if (index==5)
        this.storeson=true;      
    if (index==6)
        this.cameraon=true;          
  }

  ngOnInit() {
    
  }

  ionViewWillEnter() {
    this.authService.storage.get('is_banned').then((val) => {
       if (val==='Y')
         this.router.navigateByUrl('banned');
        });
   }

}
