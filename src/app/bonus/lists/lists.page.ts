import { Component, OnInit, ViewChild } from '@angular/core';
import { Storage } from '@ionic/storage';
import { ItemsService } from '../../items.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { toArray } from 'rxjs/operators';
import { AlertController } from '@ionic/angular';
import { AuthService } from '../../auth/auth.service';
import { Router } from '@angular/router';
import { IonContent } from '@ionic/angular';
import { TabsPage } from '../tabs/tabs.page';
import { NavController } from '@ionic/angular';


@Component({
  selector: 'app-lists',
  templateUrl: './lists.page.html',
  styleUrls: ['./lists.page.scss'],
})
export class ListsPage implements OnInit {
  public items:any;
  public alerts:any;
  public message:any;
  public alertmessage:any;
  public imagepath :string ='https://dev1-itsbonustime.tigerchef.com/assets/item_images/';
  @ViewChild('itemlist', { static: true }) itemlist; 
  @ViewChild(IonContent, { static: true }) content: IonContent;
  public active_coupons: boolean= true;
  public active_alerts: boolean= false;
  totalitems:number;  
  dataList:any;
  dataAlerts:any;
  showed:number=0;
  result:any;
  userid:number;
  SERVER_ADDRESS:  string  =  'https://dev1-itsbonustime.tigerchef.com/api/';
  loaded=true;
  renew_result:string='';


  
  constructor(public storage: Storage, public navCtrl: NavController, private tabs: TabsPage, private router: Router, public alertController: AlertController, private  httpClient:  HttpClient, private  authService:  AuthService, public itemsService: ItemsService) {
    this.dataList=[];
    this.dataAlerts=[];
    this.alerts=[];
    this.items=[];
   }

   ngOnInit() {
     this.authService.storage.get('id').then((val) => {
      this.getList(this.userid);
    });
    this.itemlist.lockSwipes(true);
  }



   getList(user_id){
    this.itemsService.loadPerUser(user_id,1)
    .then(data => {
     if (data[0].message)
        {
          this.loaded=true;
          this.message = data[0].message;

        }
      else {
        this.loaded=true;
        let allitems=data[0];
        this.items=[];
        for (let i=0;i<allitems.length;i++){
          if (allitems[i].inlist=='Y'){
              this.items.push(allitems[i]);
          }
        }
        this.resetDataList(this.items);
        this.alerts=[];
        for (let i=0;i<allitems.length;i++){
             if (allitems[i].has_alert=='Y'){
                 this.alerts.push(allitems[i]);
             }
        }
        this.resetDataAlert(this.alerts);
      }
    });
  }

  uses_left (limit, uses){
    return this.itemsService.uses_left(limit, uses);
  }

  activeAlerts(){
    this.active_coupons=false;
    this.active_alerts=true; 
    this.ionViewDidEnter();
    this.itemlist.lockSwipes(false);
    this.itemlist.slideTo(1);   
    this.itemlist.lockSwipes(true);

  }

  activeCoupons(){
    this.active_coupons=true;
    this.active_alerts=false; 
    this.ionViewDidEnter();
    this.itemlist.lockSwipes(false);
    this.itemlist.slideTo(0);   
    this.itemlist.lockSwipes(true);
  }

  loadData(event) {
    if (this.active_alerts){
          this.loadDataAlerts(event);
    } 
    else {      
      setTimeout(() => {
        for (let i = this.showed; i < this.showed+4; i++) { 
          if (this.items[i])
          this.dataList.push(this.items[i]);
        }
        event.target.complete();
      this.showed +=4;
     /* if (this.dataList.length >= this.items.length) {
        event.target.disabled = true;
      }*/
    }, 500);
   } 
  }

  loadDataAlerts(event) {
    
    setTimeout(() => {
        for (let i = this.showed; i < this.showed+4; i++) { 
          if (this.alerts[i]){
              this.dataAlerts.push(this.alerts[i]);
          }
        }
        event.target.complete();
      this.showed +=4;
      /*if (this.dataAlerts.length >= this.alerts.length) {
        event.target.disabled = true;
      }*/
    }, 500);
  }

  

  resetDataList(array){
    this.dataList = [];
    if (array)
    for (let i = 0; i < 4 && i < array.length; i++) { 
      if (array[i])
          this.dataList.push(array[i]);
    }  
    this.showed=4;
  }

  resetDataAlert(array){
    this.dataAlerts = [];
    if (array)
    for (let i = 0; i < 4 && i < array.length ; i++) { 
          if (array[i])
              this.dataAlerts.push(array[i]);
    }  
    this.showed=4;
  }


  removeAlert(item:any,event){
    let headers = new HttpHeaders();
    this.authService.storage.get('id').then((val) => {
      this.httpClient.get(`${this.SERVER_ADDRESS}items.php?action=removeAlert&itemid=${item.item_id}&userid=${val}`, {headers}).pipe(toArray())
      .subscribe(data => {
        this.result = data[0]['message'];
      });
  });

  this.addedAlert('Removed subscription','You unsubscribed from this item');
  let index = this.alerts.indexOf(item);
  if(index > -1){
    this.alerts.splice(index, 1);
  }
  index = this.items.indexOf(item);
  if(index > -1){
    this.items[index].has_alert='N';
  }
  this.resetDataAlert(this.alerts);
 }

 scrollToTop() {
  this.content.scrollToTop(400);
}

 ionViewDidEnter () {
  this.loaded=false;
  this.authService.storage.get('is_banned').then((val) => {
    if (val==='Y')
      this.router.navigateByUrl('/banned');
     });    
      this.authService.storage.get('id').then((val) => {
      this.userid = val; 
      this.getList(this.userid);
    });  
     this.tabs.changeTabInContainerPage(2);
     this.scrollToTop();
}

  async addedAlert(header:string, message:string) {
    const alert = await this.alertController.create({
      header: header,
      subHeader: '',
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }

  ViewCoupon(item) {
        item.came_from='lists';
        this.router.navigate(['tabs/deal-details', item]);
  }

  renewCoupon(coupon_id:number){
    let message:string;
    message='Coupon Renewed';
    let headers = new HttpHeaders();
    this.authService.storage.get('id').then((val) => {
      this.httpClient.get(`${this.SERVER_ADDRESS}items.php?action=renewcoupon&couponid=${coupon_id}&userid=${val}`, {headers}).pipe(toArray())
      .subscribe(data => {
        this.renew_result = data[0]['message'];
       });
  });
  for (let i=0;i<this.items.length;i++){
        if (this.items[i].coupon_id==coupon_id){
            this.items[i].hours_past=0;
        }
  }
  this.addedAlert('',message);
  }

}
