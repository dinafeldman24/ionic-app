import { Injectable } from '@angular/core';
import { Storage } from  '@ionic/storage';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './auth/auth.service';
import { toArray } from 'rxjs/operators';




@Injectable({
  providedIn: 'root'
  
})
export class ItemsService {

  data:any;
  data1:any;
  data2:any;
  SERVER_ADDRESS:  string  =  'https://dev1-itsbonustime.tigerchef.com/api/';
  public userid:any;
  limit : number =4;
  constructor(private  httpClient:  HttpClient, public  storage:  Storage, private authService:AuthService) { 

  }


  load(userid, batch) {

    return new Promise(resolve => {
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Basic " + btoa("tcbonus:bonus439"));
    this.authService.storage.get('id').then((val) => {
      if (!val)val=userid;
       this.httpClient.get(`${this.SERVER_ADDRESS}items.php?action=getitems&userid=${val}`, {headers}).pipe(toArray())
      .subscribe(data => {
        this.data = data;
        resolve(this.data);
      });
  });
   
});

}

loadPerUser(userid, peruser) {

  return new Promise(resolve => {
  let headers = new HttpHeaders();
  headers = headers.append("Authorization", "Basic " + btoa("tcbonus:bonus439"));
  this.authService.storage.get('id').then((val) => {
    if (!val)val=userid;
     this.httpClient.get(`${this.SERVER_ADDRESS}items.php?action=getlist&userid=${val}&peruser=${peruser}`, {headers}).pipe(toArray())
    .subscribe(data1 => {
      this.data1 = data1;
      resolve(this.data1);
    });
});
 
});

}

getStores() {

  return new Promise(resolve => {
  let headers = new HttpHeaders();
     headers = headers.append("Authorization", "Basic " + btoa("tcbonus:bonus439"));
     this.httpClient.get(`${this.SERVER_ADDRESS}stores.php?action=getStores`, {headers}).pipe(toArray())
    .subscribe(data2 => {
      this.data2 = data2;
      resolve(this.data2);
    });
  });
}

 
  uses_left (limit, uses){
    if (uses > 0)
      return limit-uses;
      else
      return limit;

  }
}
