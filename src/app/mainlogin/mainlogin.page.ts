import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from  "@angular/router";



@Component({
  selector: 'app-mainlogin',
  templateUrl: './mainlogin.page.html',
  styleUrls: ['./mainlogin.page.scss'],
})
export class MainloginPage implements OnInit{

 
  constructor(private  authService:  AuthService, private  router:  Router){
   
  }

  ngOnInit() {
      this.authService.storage.get('id').then((val) => {
        if (val > 0){
          this.router.navigateByUrl('tabs/deals');
        }  
      });
      
  }

}



