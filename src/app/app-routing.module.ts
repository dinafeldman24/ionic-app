import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
//  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '', loadChildren: './home/home.module#HomePageModule' },
  { path: 'signup', loadChildren: './auth/register/register.module#RegisterPageModule' },
  { path: 'login', loadChildren: './auth/login/login.module#LoginPageModule' },
  { path: 'forgotpass', loadChildren: './auth/forgotpass/forgotpass.module#ForgotpassPageModule' },
  { path: 'welcome', loadChildren: './bonus/welcome/welcome.module#WelcomePageModule' },
//  { path: 'deals', loadChildren: './bonus/deals/deals.module#DealsPageModule' },
//  { path: 'lists', loadChildren: './bonus/lists/lists.module#ListsPageModule' },
//  { path: 'redeem', loadChildren: './bonus/redeem/redeem.module#RedeemPageModule' },
//  { path: 'account', loadChildren: './bonus/account/account.module#AccountPageModule' },
  { path: 'mainlogin', loadChildren: './mainlogin/mainlogin.module#MainloginPageModule' },
  { path: 'paypal', loadChildren: './auth/paypal/paypal.module#PaypalPageModule' },
// { path: 'deal-details', loadChildren: './bonus/deal-details/deal-details.module#DealDetailsPageModule' },
  { path: 'banned', loadChildren: './banned/banned.module#BannedPageModule' },
//{ path: 'tabs', loadChildren: './bonus/tabs/tabs.module#TabsPageModule' },
  { path: 'mainlogin', loadChildren: './mainlogin/mainlogin.module#MainloginPageModule' },
  { path: '', loadChildren: './bonus/tabs/tabs.module#TabsPageModule' },
//  { path: 'results', loadChildren: './bonus/results/results.module#ResultsPageModule' },
// { path: 'new-scan', loadChildren: './new-scan/new-scan.module#NewScanPageModule' },
//  { path: 'stores', loadChildren: './bonus/stores/stores.module#StoresPageModule' },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
