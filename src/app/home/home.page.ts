import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from  "@angular/router";

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage implements OnInit{

 
  constructor(private  authService:  AuthService, private  router:  Router){
   
  }

  ngOnInit() {
     /*this.authService.storage.remove("ACCESS_TOKEN");
      this.authService.storage.remove("RESPONSE");
      this.authService.storage.remove("email");
      this.authService.storage.remove("name");
      this.authService.storage.remove("id");
      this.authService.storage.remove("access_token");*/
      this.authService.storage.remove("is_banned");
    
  }

  ngAfterViewInit (){

    this.authService.storage.forEach( (value, key, index) => {
      console.log("This is the value" + value);
      console.log("from the key "+ key);
      console.log("Index is " + index);
    });

    this.authService.storage.get('is_banned').then((val) => {
      if (val==='Y')
        this.router.navigateByUrl('/banned');
      else {
        this.authService.storage.get('id').then((val) => {
          if (val > 0){
            this.router.navigateByUrl('tabs/deals');
          } 
          else {
            this.router.navigateByUrl('welcome');
          } 
        });
      }  
  });
 

  }

  ionViewWillEnter() {
   this.authService.storage.get('is_banned').then((val) => {
      if (val==='Y')
        this.router.navigateByUrl('/banned');
       });
  }

}



