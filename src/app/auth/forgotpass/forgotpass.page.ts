import { Component, OnInit } from '@angular/core';
import { Router } from  "@angular/router";
import { AuthService } from '../auth.service';
import { FormControl, FormGroup, Validators,ValidatorFn,AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-forgotpass',
  templateUrl: './forgotpass.page.html',
  styleUrls: ['./forgotpass.page.scss'],
})
export class ForgotpassPage implements OnInit {
  
  public httpResponse: string;
  public forgotForm : FormGroup;

  constructor(private  authService:  AuthService, private  router:  Router) { }

  ngOnInit() {
    this.forgotForm = new FormGroup({
      email: new FormControl('', [Validators.required,Validators.email]),
      action: new FormControl(''),
      });

  }

  

  ionViewWillEnter() {
    this.authService.storage.get('is_banned').then((val) => {
       if (val==='Y')
         this.router.navigateByUrl('banned');
        });
    this.forgotForm.reset();   
   }

  newpass(form) {
    this.authService.forgot(form.value).subscribe((res) => {
      this.router.navigateByUrl('login');
    });
  }

  forgot () {
    this.forgotForm.controls['action'].setValue('forgotPass');
     if(this.forgotForm.valid){
       this.newpass(this.forgotForm);    
      }
    }     

}
