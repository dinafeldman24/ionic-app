import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from  "@angular/router";
import { AuthService } from '../auth.service';
import { FormControl, FormGroup, Validators,ValidatorFn,AbstractControl } from '@angular/forms';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';







@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
 

  public alreadyThere : boolean = false;
  public hasProblem : boolean = false;
  
  user : FormGroup;
  currentUser:number;
  
  constructor(private  authService:  AuthService, private  router:  Router, public fb: Facebook) { 
  }

  ngOnInit() {
    this.user = new FormGroup({
      name: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required,Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(8)]),
      action: new FormControl(''),
      });
    this.alreadyThere=false;
    this.hasProblem=false;    
  }

  
  register(form) {
    this.authService.register(form.value).subscribe((res) => {
      if (res.user.message!=='success'){
         if (res.user.message=='alreadythere')
            this.alreadyThere=true;
        else {
            this.hasProblem=true;  
            this.user.reset();  
        }
      console.log("problem");

      this.user.reset();  
    }
    else {
      this.currentUser=res.user.id;
      this.authService.storage.set("id", res.user.id);
      this.authService.storage.set("name",res.user.last_name);
      this.authService.storage.set("email",res.user.email);
      this.authService.storage.set("access_token",res.user.access_token);
      this.authService.storage.set("is_banned",res.user.is_banned);
      this.router.navigateByUrl('paypal');
      this.authService.storage.get('device_id').then((val) => {
        res.user.device_id=val;
        this.authService.sendDeviceId(res.user);
       });
      if (res.user.is_banned=='Y')
            this.router.navigate(['banned', res.user]);    
      else
            this.router.navigateByUrl('tabs/deals');
      
    }
     
    },  (err) => alert (JSON.stringify(err)));
  }



  save () {
  this.user.controls['action'].setValue('signup');
  if(!this.user.valid){
        console.log("not valid");
   }
   else {
    this.register(this.user);
  }
  
}



  ionViewWillEnter() {
    this.alreadyThere=false;
    this.hasProblem=false;  
    this.user.reset();
    this.authService.storage.get('is_banned').then((val) => {
       if (val==='Y')
         this.router.navigateByUrl('banned');
        });
   }

   facebookLogin()
   {
     const permissions = ["public_profile", "email"];
     this.fb.logout().then(() => { }).catch(() => { });
     this.fb.login(permissions).then(response =>{
       let userId = response.authResponse.userID;
 
       //Getting name and gender properties
       this.fb.api("/me?fields=name,email", permissions)
       .then(user =>{
         //user.picture = "https://graph.facebook.com/" + userId + "/picture?type=large";
         user.action='facebook';
         this.authService.login(user).subscribe((res) => {
           if (res.user.message!=='success'){
                 this.user.reset();
                 this.hasProblem=true;  
         }
         else {
           this.currentUser=res.user.id;
           this.authService.storage.set("id", res.user.id);
           this.authService.storage.set("name",res.user.last_name);
           this.authService.storage.set("email",res.user.email);
           this.authService.storage.set("is_banned",res.user.is_banned);
           this.authService.storage.set("login_type",'facebook');
           this.authService.storage.get('device_id').then((val) => {
             res.user.device_id=val;
             this.authService.sendDeviceId(res.user);
            });
           if (res.user.is_banned=='Y')
                 this.router.navigate(['banned', res.user]);    
           else
                 this.router.navigateByUrl('tabs/deals');
         }
      
         });
       })
     }, error =>{
       console.log(error);
     });
 }

}
