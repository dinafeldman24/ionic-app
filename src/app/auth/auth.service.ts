import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from  '@angular/common/http';
import { tap } from  'rxjs/operators';
import { Observable, BehaviorSubject } from  'rxjs';
import { Storage } from  '@ionic/storage';
import { User } from  './user';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  AUTH_SERVER_ADDRESS:  string  =  'https://dev1-itsbonustime.tigerchef.com/api/';
  authSubject  =  new  BehaviorSubject(false);
  user:any;

  constructor(private  httpClient:  HttpClient, public  storage:  Storage ) {  }

  register(user: User): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.append("Content-Type", "application/x-www-form-urlencoded");
    //headers= headers.append ("Access-Control-Allow-Origin", "http://localhost:8080");
    return this.httpClient.post<any>(`${this.AUTH_SERVER_ADDRESS}users.php`, user,{headers}).pipe(
      tap(async (res:  any ) => {
        this.storage.set("RESPONSE", res.user.message);
      })
    );
  }



  register2(user: User): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.append("Content-Type", "application/json");
    return this.httpClient.post<any>(`${this.AUTH_SERVER_ADDRESS}users.php`, user,{headers}).pipe(
      tap(async (res:  any ) => {
        this.storage.set("RESPONSE", res.user.message);
      })
    );
  }

  login(user: User): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.append("Content-Type", "application/x-www-form-urlencoded");
    return this.httpClient.post<any>(`${this.AUTH_SERVER_ADDRESS}users.php`, user,{headers}).pipe(
      tap(async (res:  any) => {
        if (res.user) {
          await this.storage.set("access_token",res.user.access_token);
          this.authSubject.next(true);
        }
      })
    );
  }



  forgot(user: User): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.append("Content-Type", "application/x-www-form-urlencoded");
    return this.httpClient.post<any>(`${this.AUTH_SERVER_ADDRESS}users.php`, user,{headers}).pipe(
      tap(async (res:  any ) => {
        if (res.user) {
          await this.storage.set("RESPONSE", res.user.message);
          this.authSubject.next(true);
        }
      })
    );
  }

  update(user: User): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.append("Content-Type", "application/x-www-form-urlencoded");
    return this.httpClient.post<any>(`${this.AUTH_SERVER_ADDRESS}users.php`, user,{headers}).pipe(
      tap(async (res:  any ) => {
        if (res.user) {
          await this.storage.set("RESPONSE", res.user.message);
          this.authSubject.next(true);
        }
      })
    );
  }

  updateInfo(user: User): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.append("Content-Type", "application/x-www-form-urlencoded");
    return this.httpClient.post<any>(`${this.AUTH_SERVER_ADDRESS}users.php`, user,{headers}).pipe(
      tap(async (res:  any ) => {
            console.log(JSON.stringify(res));
      })
    );
  }

  send_review(review:any): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.append("Content-Type", "application/x-www-form-urlencoded");
    return this.httpClient.post<any>(`${this.AUTH_SERVER_ADDRESS}users.php`, review,{headers}).pipe(
      tap(async (res:  any ) => {
            console.log(JSON.stringify(res));
      })
    );
  }
  

  async logout() {
    await this.storage.remove("ACCESS_TOKEN");
    this.authSubject.next(false);
  }

  isLoggedIn() {
    return this.authSubject.asObservable();
  }

  async sendDeviceId(user){
   let headers = new HttpHeaders();
    headers = headers.append("Content-Type", "application/x-www-form-urlencoded");
    headers= headers.append ("Access-Control-Allow-Origin", "http://localhost:8080");
      this.httpClient.post('https://dev1-itsbonustime.tigerchef.com/api/notifications.php',user ,{headers}).subscribe(data => {
       }, error => {
        alert(JSON.stringify(error));
      });
  }
}

