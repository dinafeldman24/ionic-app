import { Component, OnInit } from '@angular/core';
import { Router } from  "@angular/router";
import { AuthService } from '../auth.service';
import { FormControl, FormGroup, Validators, ReactiveFormsModule ,ValidatorFn,AbstractControl } from '@angular/forms';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';



@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  
  public hasProblem : boolean = false;
  public httpResponse: string;

  public loginForm : FormGroup;
  currentUser:number;
  
  constructor(private  authService:  AuthService, private  router:  Router, public fb: Facebook){}

  ngOnInit() {

    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required,Validators.email]),
      password: new FormControl('', [Validators.required]),
      action: new FormControl(''),
      });

     this.hasProblem=false;   
     
     
  }

  
  ionViewWillEnter() {
    this.authService.storage.get('is_banned').then((val) => {
       if (val==='Y')
         this.router.navigateByUrl('banned');
        });
        this.hasProblem=false;  
        this.loginForm.reset();
   }

  login(form){
    this.authService.login(form.value).subscribe((res) => {
      if (res.user.message!=='success'){
            this.loginForm.reset();
            this.hasProblem=true;  
    }
    else {
      this.currentUser=res.user.id;
      this.authService.storage.set("id", res.user.id);
      this.authService.storage.set("name",res.user.name);
      this.authService.storage.set("email",res.user.email);
      this.authService.storage.set("login_type",'pass');
      this.authService.storage.set("is_banned",res.user.is_banned);
      this.authService.storage.get('device_id').then((val) => {
        res.user.device_id=val;
        this.authService.sendDeviceId(res.user);
       });
      if (res.user.is_banned=='Y')
            this.router.navigate(['banned', res.user]);    
      else
            this.router.navigateByUrl('tabs/deals');
    }
 
    });
  }


  save () {
   this.loginForm.controls['action'].setValue('login');
    if(this.loginForm.valid){
      this.login(this.loginForm);    
     } 
  }

  facebookLogin()
    {
     // const permissions = ["public_profile", "email"];
     const permissions = ["public_profile"];
      this.fb.logout().then(() => { }).catch(() => { });
      this.fb.login(permissions).then(response =>{
        let userId = response.authResponse.userID;
  
        //Getting name and gender properties
        this.fb.api("/me?fields=name,email", permissions)
        .then(user =>{
          //user.picture = "https://graph.facebook.com/" + userId + "/picture?type=large";
          user.action='facebook';
          this.authService.login(user).subscribe((res) => {
            if (res.user.message!=='success'){
                  this.loginForm.reset();
                  this.hasProblem=true;  
          }
          else {
            this.currentUser=res.user.id;
            this.authService.storage.set("id", res.user.id);
            this.authService.storage.set("name",res.user.last_name);
            this.authService.storage.set("email",res.user.email);
            this.authService.storage.set("is_banned",res.user.is_banned);
            this.authService.storage.set("login_type",'facebook');
            this.authService.storage.get('device_id').then((val) => {
              res.user.device_id=val;
              this.authService.sendDeviceId(res.user);
             });
            if (res.user.is_banned=='Y')
                  this.router.navigate(['banned', res.user]);    
            else
                  this.router.navigateByUrl('tabs/deals');
          }
       
          });
        })
      }, error =>{
        console.log(JSON.stringify(error));
      });
  }
}

