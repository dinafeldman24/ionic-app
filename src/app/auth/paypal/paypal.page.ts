import { Component, OnInit } from '@angular/core';
import { PayPal, PayPalPayment, PayPalConfiguration,PayPalPaymentDetails } from '@ionic-native/paypal/ngx';
import { NavController } from '@ionic/angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from '../auth.service';
import { AlertController } from '@ionic/angular';
import { Router , ActivatedRoute} from  "@angular/router";





@Component({
  selector: 'app-paypal',
  templateUrl: './paypal.page.html',
  styleUrls: ['./paypal.page.scss'],
})
export class PaypalPage implements OnInit {
  AUTH_SERVER_ADDRESS:  string  =  'https://dev1-itsbonustime.tigerchef.com/api/';
  message:any;
  lastpage:string;
  data:any;
  loading=false;
  constructor(private route: ActivatedRoute, private  router:  Router, public alertController: AlertController, private  authService:  AuthService, private  httpClient:  HttpClient, public payPal: PayPal, public navCtrl:NavController) {
    this.route.queryParams.subscribe(params => {
      if (params && params.frompage) {
        this.data = params.frompage;
      }
    });
  }

  ngOnInit() {
  }

  paypal(){
    this.payPal.init({
        PayPalEnvironmentProduction: '',
        PayPalEnvironmentSandbox: 'AeWja13Ni5n92wHjSNsNhaoMYogHD2vOSuBpLJjCFpVS9VaCprDHm3_49wByjJdIp5SqSTw01MHkBYtM'
    }).then(() => {
      this.payPal.prepareToRender('PayPalEnvironmentSandbox', new PayPalConfiguration({
        acceptCreditCards: true,
        merchantName: 'Bonus',
        merchantPrivacyPolicyURL: 'https://dev1-itsbonustime.tigerchef.com/',
        merchantUserAgreementURL: 'https://dev1-itsbonustime.tigerchef.com/'
      })).then(() => {
    this.payPal.renderProfileSharingUI(['email']).then((response) => {
        this.authService.storage.get('id').then((val) => {
            this.loading=true;
            response.id=val;
            this.sendToPaypal(response);
        });
        
        }, (err) => {
          alert(err);
        })
      })
    })
  }

 async sendToPaypal(response){
    let headers = new HttpHeaders();
    headers = headers.append("Content-Type", "application/x-www-form-urlencoded");
    headers= headers.append ("Access-Control-Allow-Origin", "http://localhost:8080");
      this.httpClient.post('https://dev1-itsbonustime.tigerchef.com/api/paypal.php',response ,{headers}).subscribe(data => {
        if(data){
            this.loading=true;
            this.message=data[0].message;
            if (data[0].is_banned=='Y')
                {
                  this.authService.storage.set("is_banned",'Y');
                  this.router.navigateByUrl('/banned');
                }
            this.addedAlert(this.message);
            if (this.data=='account')
                this.router.navigateByUrl('tabs/account');
            else
                this.router.navigateByUrl('welcome');
        }
       }, error => {
        alert(JSON.stringify(error));
      });
  }

  async addedAlert(message) {
    const alert = await this.alertController.create({
      header: 'Paypal',
      subHeader: '',
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }

  
  ionViewWillEnter() {
    this.authService.storage.get('is_banned').then((val) => {
       if (val==='Y')
         this.router.navigateByUrl('banned');
        });
   }
}


