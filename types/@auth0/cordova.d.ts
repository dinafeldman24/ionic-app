export = index;
declare class index {
  static onRedirectUri(url: any): any;
  static version: string;
  constructor(options: any);
  clientId: any;
  domain: any;
  redirectUri: any;
  auth0: any;
  client: any;
  authorize(parameters: any, callback: any): void;
}
